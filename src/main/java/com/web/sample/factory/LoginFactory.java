package com.web.sample.factory;
  
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;

import com.web.sample.utility.BaseclassWeb;

	public class LoginFactory extends BaseclassWeb{

		//This is a constructor, as every page need a base driver to find web elements
		public LoginFactory(WebDriver driver) {
			super();
		}
		
		

		@FindBy(how = How.XPATH, using = "//input[@id='exampleInputEmail']")     
		public WebElement input_UserName ;
		  
		@FindBy(how = How.XPATH, using = "//input[@id='exampleInputPassword']")     
		public WebElement input_Password ;
		
		@FindBy(how = How.XPATH, using = "//button[@class='g-recaptcha btn btn-primary']")     
		public WebElement button_Login ;

		public WebElement getInput_UserName() {
			return input_UserName;
		}

		public void setInput_UserName(WebElement input_UserName) {
			this.input_UserName = input_UserName;
		}

		public WebElement getInput_Password() {
			return input_Password;
		}

		public void setInput_Password(WebElement input_Password) {
			this.input_Password = input_Password;
		}

		public WebElement getButton_Login() {
			return button_Login;
		}

		public void setButton_Login(WebElement button_Login) {
			this.button_Login = button_Login;
		}

		
	}
